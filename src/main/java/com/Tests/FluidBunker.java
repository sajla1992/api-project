package com.Tests;

import java.lang.reflect.Method;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.BusinessLogic.Event_BusinessLogic;
import com.BusinessLogic.FluidBunker_BusinessLogic;
import com.BusinessLogic.Report_BusinessLogic;
import com.InitialSetup.BaseClass;
import com.Reporting.ExtentTestManager;
import com.aventstack.extentreports.ExtentTest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class FluidBunker extends BaseClass {
	public static String tcID = null;
	private ExtentTest rootTest;
	private ExtentTest test;
	private String tokenID;

	@BeforeClass
	
	public void setClass() {
//		getTestData();
		rootTest = ExtentTestManager.startTest(getClass().getSimpleName());
		
	}
	
	@BeforeMethod
	public void setUp(Method method) throws Exception {
		System.out.println(method.getName());
		testCaseName = method.getName();
		test = ExtentTestManager.createChildNode(rootTest, method.getName());
	}
	
	
@Test(priority =1 , description = "create position")
	public void createPositionVerify(Method method) throws Throwable {
		FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
		getBaseUrl(testCaseName, "reterive host url");
		String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
		Response response=test.createPosition(testCaseName, "create position", resourceUrl);
		verifyResponseCode(response, 200, "verify response status code");
		getHeader(response);	
		JsonPath js = new  JsonPath(response.asString());
	    setProperty("sfpM_Form_Id", js.getInt("sfpM_Form_Id"));	
	    setProperty("voyageNo", js.getString("voyageNo"));
	}
@Test(priority =1 , description = "Create fluid bunker")
	public void createFluidBunkerVerify(Method method) throws Throwable {
	FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
		getBaseUrl(testCaseName, "reterive host url");
		String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
		Response response=test.createFluidBunker(testCaseName, "Create fluid bunker", resourceUrl);
	    verifyResponseCode(response, 200, "verify response status code");
	    getHeader(response);
	    JsonPath js = new  JsonPath(response.asString());
		setProperty("robId", js.getInt("robId"));	
		setProperty("fluidType", js.getString("fluidType"));
	} 
@Test(priority =2 , description = "Create fluid bunker with duplicate data")
    public void createFluidBunkerDuplicateDataVerify(Method method) throws Throwable {
    FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
	    getBaseUrl(testCaseName, "reterive host url");
	    String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	    Response response=test.createFluidBunkerDuplicateData(testCaseName, "Create fluid bunker with duplicate data", resourceUrl);
        verifyResponseCode(response, 400, "verify response status code");
        getHeader(response);
    } 
@Test(priority =3 , description = "Create fluid bunker with balnk data")
    public void createFluidBunkerBlankValueVerify(Method method) throws Throwable {
    FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
	    getBaseUrl(testCaseName, "reterive host url");
	    String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	    Response response=test.createFluidBunkerBlankValue(testCaseName, "Create fluid bunker with blank data", resourceUrl);
        verifyResponseCode(response, 400, "verify response status code");
        getHeader(response);
    }
@Test(priority =4 , description = "update fluid bunker")
    public void updateFluidBunkerVerify(Method method) throws Throwable {
    FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
        getBaseUrl(testCaseName, "reterive host url");
        String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
        Response response=test.updateFluidBunker(testCaseName, "update fluid bunker", resourceUrl);
        verifyResponseCode(response, 200, "verify response status code");
        getHeader(response);
    }
@Test(priority =5 , description = "check the fluid bunker")
public void getFluidBunkerbyIdVerify(Method method) throws Throwable {
FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
    getBaseUrl(testCaseName, "reterive host url");
    String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
    Response response=test.getFluidBunkerbyId(testCaseName, "check the fluid bunker", resourceUrl);
    verifyResponseCode(response, 200, "verify response status code");
    getHeader(response);
}
@Test(priority =6 , description = "check all the fluid bunker")
public void getAllFluidBunkerVerify(Method method) throws Throwable {
FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
    getBaseUrl(testCaseName, "reterive host url");
    String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
    Response response=test.getAllFluidBunker(testCaseName, "check all the fluid bunker", resourceUrl);
    verifyResponseCode(response, 200, "verify response status code");
    getHeader(response);
}
@Test(priority =7 , description = "delete fluid consumption")
public void deleteFluidConsumptionVerify(Method method) throws Throwable {
FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
    getBaseUrl(testCaseName, "reterive host url");
    String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
    Response response=test.deleteFluidConsumption(testCaseName, "delete fluid consumption", resourceUrl);
    verifyResponseCode(response, 200, "verify response status code");
    getHeader(response);
}
@Test(priority=8, description="get all event ")
public void getAllEventsVerify(Method method) throws Throwable {
	FluidBunker_BusinessLogic test=new FluidBunker_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response =test.getAllEvents(testCaseName, "get all event ", resourceUrl);
    verifyResponseCode(response, 200, "verify response status code");
    getHeader(response);
}
    
}
