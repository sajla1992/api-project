package com.Tests;

import java.lang.reflect.Method;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.BusinessLogic.PositionWarning_BusinessLogic;
import com.BusinessLogic.Report_BusinessLogic;
import com.BusinessLogic.Voyages_BusinessLogic;
import com.InitialSetup.BaseClass;
import com.Reporting.ExtentTestManager;
import com.aventstack.extentreports.ExtentTest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Report extends BaseClass {
	public static String tcID = null;
	private ExtentTest rootTest;
	private ExtentTest test;
	private String tokenID;

	@BeforeClass
	
	public void setClass() {
//		getTestData();
		rootTest = ExtentTestManager.startTest(getClass().getSimpleName());
		
	}
	
	@BeforeMethod
	public void setUp(Method method) throws Exception {
		System.out.println(method.getName());
		testCaseName = method.getName();
		test = ExtentTestManager.createChildNode(rootTest, method.getName());
	}
	
@Test(priority =1 , description = "create voyages")
	
	public void createVoyageVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
		getBaseUrl(testCaseName, "reterive host url");
		String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
		Response response=test.createVoyage(testCaseName, "create voyages", resourceUrl);
	    verifyResponseCode(response, 200, "verify response status code");
	    getHeader(response);
	    JsonPath js = new  JsonPath(response.asString());
		setProperty("sfpM_VoyagesId", js.getInt("sfpM_VoyagesId"));	
		setProperty("imoNumber", js.getInt("imoNumber"));	
		setProperty("voyageNumber", js.getString("voyageNumber"));	
		verifyResponseBodyContent(response.asString());
		verifyResponseBodyContentSequence(response.asString());
	    verifyResponseBodyContentDatatype(response.asString());	
	} 
@Test(priority =2 , description = "Create PassageReportExclusion")
public void createPassageReportExclusionVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response=test.createPassageReportExclusion(testCaseName, "Create PassageReportExclusion", resourceUrl);
    verifyResponseCode(response, 200, "verify response status code");
    getHeader(response);
 
    } 
@Test(priority =3 , description = "check passage exclusion recorpt")
public void getPassageReportExclusionVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response=test.getPassageReportExclusion(testCaseName, "check passage exclusion recorpt", resourceUrl);
    verifyResponseCode(response, 200, "verify response status code");
    getHeader(response);
 
    } 
@Test(priority =4 , description = "check passage exclusion recorpt log")
public void getPassageReportExclusionLogVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response=test.getPassageReportExclusion(testCaseName, "check passage exclusion recorpt log", resourceUrl);
    verifyResponseCode(response, 200, "verify response status code");
    getHeader(response);
 
    } 
@Test(priority =5 , description = "create position")
public void createPositionVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response=test.createPosition(testCaseName, "create position", resourceUrl);
	verifyResponseCode(response, 200, "verify response status code");
	getHeader(response);	
	JsonPath js = new  JsonPath(response.asString());
    setProperty("sfpM_Form_Id", js.getInt("sfpM_Form_Id"));	
    setProperty("voyageNo", js.getString("voyageNo"));
}
@Test(priority =6 , description = "create position report exclusion")
public void createPositionReportExclusionVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response=test.createPositionReportExclusion(testCaseName, "create position report exclusion", resourceUrl);
	verifyResponseCode(response, 200, "verify response status code");
	getHeader(response);	
}
@Test(priority =7 , description = "get position report exclusion")
public void getPositionReportExclusionVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response=test. getPositionReportExclusion(testCaseName, "get position report exclusion", resourceUrl);
	verifyResponseCode(response, 200, "verify response status code");
	getHeader(response);	
}
@Test(priority =8 , description = "get position report exclusion log")
public void getPositionReportExclusionLogVerify(Method method) throws Throwable {
	Report_BusinessLogic test=new Report_BusinessLogic(driver, testCaseName);
	getBaseUrl(testCaseName, "reterive host url");
	String resourceUrl=getResourceUrl(testCaseName, "reterive base url");
	Response response=test. getPositionReportExclusionLog(testCaseName, "get position report exclusion log", resourceUrl);
	verifyResponseCode(response, 200, "verify response status code");
	getHeader(response);	
}


}
