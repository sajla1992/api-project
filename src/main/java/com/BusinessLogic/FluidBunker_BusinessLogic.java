package com.BusinessLogic;

import static io.restassured.RestAssured.given;

import org.openqa.selenium.WebDriver;

import com.InitialSetup.BaseClass;
import com.PageObject.Scorpio_PageObject;
import com.Reporting.ExtentTestManager;
import com.TestData.Excel_Handling;
import com.google.gson.Gson;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class FluidBunker_BusinessLogic extends BaseClass {
	public WebDriver driver;
	public  String testCaseName = "";
	public JsonPath js;
	public String convertAsStr;
	Scorpio_PageObject object = new Scorpio_PageObject(driver, testCaseName);
	public Response res;

	public FluidBunker_BusinessLogic(WebDriver driver, String testCaseName)
	{
		this.driver = driver; 
		this.testCaseName=testCaseName;
	}
	@SuppressWarnings("unused")
	
	public Response createPosition(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "Create position");
		setBody(object.formIdentifier, getRandomName());
		setBody(object.reportDateTime, "2020-01-21T12:09");
		setBody(object.latitude, getRandomNumber());
		setBody(object.longitude, getRandomNumber());
		setBody(object.steamingHrs, getRandomName());
		setBody(object.fwdDraft, getRandomName());
		setBody(object.distanceToGO, getRandomNumber());
		setBody(object.engineDistance,getRandomNumber());
    	setBody(object.slip, getRandomNumber());
    	setBody(object.shaftPower, getRandomNumber());
    	setBody(object.windDirection, getRandomNumber());
    	setBody(object.seaDir, getRandomNumber());
    	setBody(object.voyageNo, getRandomNumber());
    	//System.out.println(new Gson().toJson(getBody()));
		Response res=given().header("Content-Type", "application/json").body(new Gson().toJson(getBody())).
				when().
				post(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response createFluidBunker(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "Create fluid bunker");
		setBody(object.formId, getProperty("sfpM_Form_Id"));
		setBody(object.fluidType, getRandomName());
		setBody(object.unit, getRandomName());
		setBody(object.bunkerType, Excel_Handling.Get_Data(testCaseName, "bunkerType"));
		setBody(object.consumption, getRandomNumber());
		//System.out.println(new Gson().toJson(getBody()));
		Response res=given().header("Content-Type", "application/json").body(new Gson().toJson(getBody())).
				when().
				post(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response createFluidBunkerDuplicateData(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "Create fluid bunker with duplicate data");
		setBody(object.formId, getProperty("sfpM_Form_Id"));
		setBody(object.fluidType, getProperty("fluidType"));
		setBody(object.unit, getRandomName());
		setBody(object.bunkerType, Excel_Handling.Get_Data(testCaseName, "bunkerType"));
		setBody(object.consumption, getRandomNumber());
		//System.out.println(new Gson().toJson(getBody()));
		Response res=given().header("Content-Type", "application/json").body(new Gson().toJson(getBody())).
				when().
				post(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response createFluidBunkerBlankValue(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "Create fluid bunker with duplicate data");
		//System.out.println(new Gson().toJson(getBody()));
		Response res=given().header("Content-Type", "application/json").body(new Gson().toJson(getBody())).
				when().
				post(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response updateFluidBunker(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "update fluid bunker");
		setBody(object.formId, getProperty("sfpM_Form_Id"));
		setBody(object.fluidType, getRandomName());
		setBody(object.unit, getRandomName());
		setBody(object.bunkerType, Excel_Handling.Get_Data(testCaseName, "bunkerType"));
		setBody(object.consumption, getRandomNumber());
		setBody(object.robId, getProperty("robId"));
		//System.out.println(new Gson().toJson(getBody()));
		Response res=given().header("Content-Type", "application/json").body(new Gson().toJson(getBody())).
				when().
				put(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response getFluidBunkerbyId(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "check the fluid bunker");
		//System.out.println(new Gson().toJson(getBody()));
		Response res=given().queryParam(object.formId, getProperty("sfpM_Form_Id")).queryParam(object.bunkerType, Excel_Handling.Get_Data(testCaseName, "bunkerType")).
				when().
				get(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response getAllFluidBunker(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "check all the fluid bunker");
		//System.out.println(new Gson().toJson(getBody()));
		Response res=given().queryParam(object.formId, getProperty("sfpM_Form_Id")).
				when().
				get(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response deleteFluidConsumption(String testCaseID,String stepName, String resourceUrl ) throws Exception {
		ExtentTestManager.startlog(testCaseID, stepName, "delete fluid consumption");
		//System.out.println(new Gson().toJson(getBody()));
		Response res=given().queryParam(object.formId, getProperty("sfpM_Form_Id")).queryParam(object.bunkerType, Excel_Handling.Get_Data(testCaseName, "bunkerType")).queryParam(object.robId, getProperty("robId")).
				when().
				delete(resourceUrl).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response " + convertAsStr );	
		return res;	
	}
	public Response getAllEvents(String testCaseID, String stepNmae, String resourceURL) throws Exception { 
		ExtentTestManager.startlog(testCaseID, stepNmae, "get all event ");
		Response res=given().queryParam(object.formId, getProperty("formId")).
				when().
				get(resourceURL).
				then().extract().response();
		convertAsStr=res.asString();
		System.out.println("Response" + convertAsStr);
		return res;	
	}
	
	
	
}
